#ifndef GEO_GLSL_UTILS_H_
#define GEO_GLSL_UTILS_H_

#include <glad.h>
#include <cassert>
#include <cstdio>
#include <string>

namespace geo
{
namespace glsl
{
inline bool isShaderCompileSucceed(GLuint shader)
{
    GLint isSuccess;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isSuccess);
    return isSuccess == GL_TRUE;
}

inline std::string getShaderInfoLog(GLuint shader)
{
    GLint logBytes = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logBytes);

    std::string infoLog(logBytes, '\0');
    glGetShaderInfoLog(shader, logBytes, nullptr, infoLog.data());

    return infoLog;
}

inline GLuint createShaderFromSource(GLenum type, const char* source)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, nullptr);
    glCompileShader(shader);
    bool isSucceed = isShaderCompileSucceed(shader);
    if (!isSucceed) { std::printf("%s\n", getShaderInfoLog(shader).c_str()); }
    assert(isSucceed);
    return shader;
}

inline bool isProgramLinkSucceed(GLuint program)
{
    GLint isSuccess;
    glGetProgramiv(program, GL_LINK_STATUS, &isSuccess);
    return isSuccess == GL_TRUE;
}

inline std::string getProgramInfoLog(GLuint program)
{
    GLint logBytes = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logBytes);

    std::string infoLog(logBytes, '\0');
    glGetProgramInfoLog(program, logBytes, nullptr, infoLog.data());

    return infoLog;
}

inline GLuint createProgramFromShaders(GLuint vs, GLuint fs)
{
    GLuint program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    bool isSucceed = isProgramLinkSucceed(program);
    if (!isSucceed) { std::printf("%s\n", getProgramInfoLog(program).c_str()); }
    assert(isSucceed);
    return program;
}

inline GLuint createShaderProgramFromSource(const char* vsSource, const char* fsSource)
{
    GLuint vertexShader = createShaderFromSource(GL_VERTEX_SHADER, vsSource);
    GLuint fragmentShader = createShaderFromSource(GL_FRAGMENT_SHADER, fsSource);

    GLuint shaderProgram = createProgramFromShaders(vertexShader, fragmentShader);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return shaderProgram;
}
}  // namespace glsl
}  // namespace geo

#endif  // GEO_GLSL_UTILS_H_
